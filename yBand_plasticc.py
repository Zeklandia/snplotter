import matplotlib.pyplot as plt
import sncosmo,os,glob
import numpy as np
from optparse import OptionParser

sed_directory=os.path.join('..','SED_yBand_extrapolations')
old_sed_directory=os.path.join('..','..','Downloads','SNDATA_ROOT','snsed','non1a')

def plotBoxLC(sed=None,boxStart=9200,boxWidth=200,snType='Ib',phaseRange=(-15,50)):
    if isinstance(sed,str):
        title=sed
    elif isinstance(sed,(tuple,list)) and len(sed)==1:
        title=sed[0]
    else:
        title='Type '+snType
    wave=[boxStart,boxStart+boxWidth] 
    transmission=[1,1]
    sncosmo.registry.register(sncosmo.Bandpass(wave,transmission,name='windowFilter'),force=True)
    if not sed:
        sedList=glob.glob(os.path.join(sed_directory,'Type_'+snType,'*.SED'))
    else:
        sedList=[sed] if not isinstance(sed,(tuple,list)) else sed
    sedList=[os.path.basename(x) for x in sedList]
    
        
    
    if len(sedList)==1:
        fig,ax=plt.subplots(nrows=1,ncols=2,sharex=True,sharey=False,figsize=(10,5))
        if not sedList[0].endswith('.SED'):
            sedList[0]+='.SED'
        for j in range(2):
            if j==1:
                phase,wave,flux=sncosmo.read_griddata_ascii(os.path.join(sed_directory,'Type_'+snType,os.path.basename(sedList[0])))
            else:
                phase,wave,flux=sncosmo.read_griddata_ascii(os.path.join(old_sed_directory,sedList[0]))
                
            source=sncosmo.TimeSeriesSource(phase,wave,flux)
            mod=sncosmo.Model(source)
            mod.set(z=.02)
            try:
                ax[j].plot(np.arange(phaseRange[0],phaseRange[1],.5),
                              mod.bandflux('windowFilter',np.arange(phaseRange[0],phaseRange[1],.5)))
            except:
                ax[j].annotate('Original Template Did Not Cover This Range',size=14,xy=(.15,.5), xycoords='axes fraction')
            ax[j].annotate(os.path.basename(sedList[0])[:-4],size=14,xy=(.65,.75), xycoords='axes fraction')

        ax[0].set_title('Original SNANA SEDs',fontsize=16)
        ax[1].set_title('Pierel 2018 SEDs',fontsize=16)
    else:
        fig,ax=plt.subplots(nrows=len(sedList),ncols=2,sharex=True,sharey=False,
                            figsize=(max(2*len(sedList),10),max(2.5*len(sedList),10)))
        for i in range(len(sedList)):
            if not sedList[i].endswith('.SED'):
                sedList[i]+='.SED'
            for j in range(2):
                if j==1:
                    phase,wave,flux=sncosmo.read_griddata_ascii(os.path.join(sed_directory,'Type_'+snType,os.path.basename(sedList[i])))
                else:
                    phase,wave,flux=sncosmo.read_griddata_ascii(os.path.join(old_sed_directory,sedList[i]))

                source=sncosmo.TimeSeriesSource(phase,wave,flux)
                mod=sncosmo.Model(source)
                mod.set(z=.02)
                try:
                    ax[i][j].plot(np.arange(phaseRange[0],phaseRange[1],.5),
                                  mod.bandflux('windowFilter',np.arange(phaseRange[0],phaseRange[1],.5)))
                except:
                    ax[i][j].annotate('Original Template Did Not Cover This Range',size=14,xy=(.15,.5), xycoords='axes fraction')
                ax[i][j].annotate(os.path.basename(sedList[i])[:-4],size=14,xy=(.65,.75), xycoords='axes fraction')

        ax[0][0].set_title('Original SNANA SEDs',fontsize=16)
        ax[0][1].set_title('Pierel 2018 SEDs',fontsize=16)
    plt.suptitle('Y Band Comparison--'+title,fontsize=22)
    plt.show()
    plt.close()

def findSNType(filename):
    for typ in ['Ib','Ic','II']:
        files=glob.glob(os.path.join(sed_directory,'Type_'+typ,'*.SED'))
        for f in files:
            if filename in f:
                return(typ)
def callback(option, opt, value, parser):
  setattr(parser.values, option.dest, value.split(','))        

def main():
    parser = OptionParser()
    parser.add_option("--file", type='string',action='callback',dest="inputfile",callback=callback,default=None)
    parser.add_option("--start", type='int',dest="start", default=9200)
    parser.add_option("--width", type='int',dest="width", default=200)
    parser.add_option("--snType", type='string',dest="typ", default=None)

    (options, args) = parser.parse_args()
    
    if options.inputfile is not None and options.typ is None:
        options.typ=findSNType(options.inputfile[0] if isinstance(options.inputfile,(tuple,list)) else options.inputfile)
    elif options.inputfile is None:
        options.typ='Ib'

    plotBoxLC(options.inputfile,snType=options.typ,boxStart=options.start,boxWidth=options.width)


if __name__ == '__main__':
    main()


